import matplotlib.pyplot as plt 
import csv

match_years = []
match_count = {}
sorted_count = []

with open('matches.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)
	for row in match_reader:
		year = int(row[1])
		if year in match_count.keys():
			match_count[year] += 1
		else:
			match_count[year] = 1

match_years = sorted(match_count.keys()) 
for yr in match_years:
	sorted_count.append(match_count[yr])

left = range(len(match_count.keys()))
height = sorted_count
plt.bar(left, height, tick_label = match_years, width = 0.8, color = 'gray')  
plt.xlabel("IPL YEARS")
plt.ylabel("TOTAL MATCHES") 
plt.title('IPL MATCH CHART PER YEAR')   
plt.show()