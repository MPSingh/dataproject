import matplotlib.pyplot as plt 
import csv

year_list = []
win_year = {}
total_teams = [] 
totalwin_perteam = {}
final_teams=[]
final_wins = []	

with open('matches.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)
	for row in match_reader:
		year_list.append(row[1])
		if row[10] != '' and row[10] not in total_teams:
			total_teams.append(row[10])
	year_list = sorted(set(year_list))

with open('matches.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)	
	for year in year_list:
		win_count = {}
		for row in match_reader:
			if year == row[1]  and row[10] != '':
				team = row[10]
				if team not in win_count.keys():
					win_count[team] = 1
				else:
					win_count[team] += 1
			win_year[year] = win_count
		info.seek(0)

for team in total_teams:
	win_team = []
	for yr in year_list:
		year_team = list(win_year[yr].keys())
		if team in year_team:
			win_team.append(win_year[yr][team])
		else:
			win_team.append(0)
	totalwin_perteam[team] = win_team

for k, v in totalwin_perteam.items():
	final_teams.append(k)
	final_wins.append(v)
print(final_teams)
print(final_wins)

width = 0.35
ind = range(len(year_list))
colors = ['blue','pink', 'chocolate', 'blueviolet', 'green', 'red', 'crimson', 'lime', 'black', 'aliceblue', 'brown', 'yellow', 'orange', 'burlywood']
plt.bar(ind, final_wins[0],width, color=colors[0])
bottom_li = final_wins[0]

def total_height(li1,li2):
	bottom = [sum(pair) for pair in zip(li1,li2)]
	return bottom

for i in range(1, 14):
	plt.bar(ind, final_wins[i], width, color=colors[i], bottom=bottom_li)
	bottom_li=total_height(bottom_li,final_wins[i])

plt.ylabel('TotalWin/Team')
plt.xlabel('WiningTeam/Year')
plt.title('Teams Won per Year')
plt.xticks(ind, year_list)
plt.legend(final_teams, loc=9, bbox_to_anchor=(0.5,1.15), ncol=5)
plt.show()