import matplotlib.pyplot as plt 
import numpy as np
import csv

venue_count = {}
venue_list = []
top_10_names = []
top_10_values = []
count = 1

with open('matches.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)
	for row in match_reader:
		venue = row[14]
		if venue not in venue_count.keys():
			venue_count[venue] = 1
		else:
			venue_count[venue] += 1

sorted_values = sorted(venue_count.values(),reverse = True)
for value in sorted_values:
	for name in venue_count.keys():
		if value == venue_count[name] and count<=10:
			top_10_names.append(name)
			top_10_values.append(value)
			count += 1

venue_names = top_10_names
height = top_10_values
left = range(len(top_10_values))
plt.bar(left, height, tick_label=venue_names, width = 0.5, color = ["green" ,"red", "indigo", "yellow", "blue", "pink", "magenta", "orange", "lime","brown"])
plt.xticks(rotation=-70)  
plt.xlabel("Venues")
plt.ylabel("Number of Matches") 
plt.title('TOP 10 VENUES of IPL SEASONS')
plt.show()