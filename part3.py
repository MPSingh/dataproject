import matplotlib.pyplot as plt 
import numpy as np
import csv

bowling_team_2016 = []
extra_per_team = {}
Id = []

with open('matches.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)
	for row in match_reader:
		if row[1] =='2016':
			Id.append(row[0])

with open('deliveries.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)
	for row in match_reader:
		team = row[3]
		if team not in bowling_team_2016: 
			bowling_team_2016.append(team)
print(bowling_team_2016)	
			
with open('deliveries.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)	
	for team in bowling_team_2016:
		for row in match_reader:
			match_id = row[0]
			if team == row[3] and match_id in Id:
				if team not in extra_per_team.keys():
					extra_per_team[team] = 0
				else:
					extra_per_team[team] += int(row[16])
		info.seek(0)

teams = extra_per_team.keys()
left = range(len(teams))
height = list(extra_per_team.values())
plt.bar(left, height, tick_label = teams, width = 0.5, color = 'green')  
plt.xlabel("IPL TEAMS - 2016")
plt.ylabel("TOTAL EXTRA CONCEDED") 
plt.title('IPL MATCH CHART 2016')   
plt.show()