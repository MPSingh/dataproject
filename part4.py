import matplotlib.pyplot as plt 
import csv

Id = []
bowler_economy = {}
bowlers = []
count = 1
top_10_bowlers = []
top_10_economy = []

with open('matches.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)
	for row in match_reader:
		if row[1] =='2015':
			Id.append(row[0])

with open('deliveries.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)	
	for row in match_reader:
		match_id = row[0]
		if match_id in Id:
			bowler = row[8]
			if bowler not in bowlers:
				bowlers.append(bowler)

with open('deliveries.csv') as info:
	match_reader = csv.reader(info, delimiter=',')
	next(match_reader)
	for bowler in bowlers:
		total_balls = 0
		total_runs = 0
		for row in match_reader:
			row_bowler = row[8]
			match_id = row[0]
			run = row[17]			
			if match_id in Id and bowler == row_bowler:
				total_balls += 1
				total_runs += int(run)
		bowler_economy[bowler] = round((total_runs/total_balls) * 6,2)
		info.seek(0)

sorted_values = sorted(bowler_economy.values(),reverse = True)
for value in sorted_values:
	for bowler in bowler_economy.keys():
		if value == bowler_economy[bowler] and count<=10:
			top_10_bowlers.append(bowler)
			top_10_economy.append(value)
			count += 1
# print(top_10_economy)
# print(top_10_bowlers)
bowler_names = top_10_bowlers
height = top_10_economy
left = range(len(top_10_economy))
plt.bar(left, height, tick_label=bowler_names, width = 0.5, color = ["green" ,"red", "indigo", "yellow", "blue", "pink", "magenta", "orange", "lime","brown"])
plt.xticks(rotation=-70)  
plt.xlabel("BOWLERS")
plt.ylabel("ECONOMY/BOWLER") 
plt.title('TOP 10 ECONOMICAL BOWLERS OF IPL 2015')
plt.show()
